package com.adfinbureau.wms.service.oracle;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.adfinbureau.wms.domain.Location;
import com.adfinbureau.wms.repository.LocationRepository;
import com.adfinbureau.wms.web.rest.dto.oracle.GispdamPhotoCollect;
import com.adfinbureau.wms.web.rest.dto.oracle.GispdamResponse;

/**
 * REST controller for managing Location.
 */
@Service
public class GispdamService {

    private final Logger log = LoggerFactory.getLogger(GispdamService.class);

    @Autowired
    LocationRepository locationRepository;
    
    RestTemplate restTemplate = new RestTemplate();
    
    @Scheduled(cron = "1 * * * * ?")
    public void test(){
    	log.debug("TEST 1 | EVERY 1 second or 1 minutes");
    }
    
//    @Scheduled(cron = "*/1 * * * * ?")
//    public void test2(){
//    	log.debug("TEST 2 | EVERY 1 second or 1 minutes");
//    }

    @Scheduled(cron = "1 * * * * ?")
	public void sendToServer(){    	
    	List<Location> locations = locationRepository.findAllByStatus("PENDING");
    	log.debug("SEND TO SERVER "+locations.size());
    	locations.forEach((value) -> {
        	GispdamPhotoCollect x = new GispdamPhotoCollect();
        	if(value.getType() != null){
        		log.debug("LOCATION TYPE NOT NULL");
	    		if(value.getType().contentEquals("PICTURE")){
	    			if(value.getPicture() != null){
	    				x.setPicture(value.getPicture());
	    			}
	    			else{
	    				return;
	    			}
	    		}
	    		else if(value.getType().contentEquals("VIDEO")){
	    			if(value.getVideo() != null){
	    				x.setVideo(value.getVideo());
	    			}
	    			else{
	    				return;
	    			}    			
	    		}
	    		else if(value.getType().contentEquals("ASBUILD")){
	    			if(value.getAsbuild() != null){
	    				x.setAsbuild(value.getAsbuild());
	    			}
	    			else{
	    				return;
	    			}    			    			
	    		}
	    		else if(value.getType().contentEquals("MASALAH")){
	            	x.setMasalah(value.getMasalah());
	            	x.setUsulan(value.getUsulan());
	    		}
	    		
	        	x.setIdstyle("Android");
	        	x.setType(value.getType());
	        	x.setLatitude(value.getLatitude());
	        	x.setLogitude(value.getLongitude()); 
	        	
	        	if(value.getAltitude() != null){
	        		x.setElevasi(value.getAltitude());
	        	}else{
	        		x.setElevasi("0");
	        	}
	        	x.setName(value.getDescription());
	        	x.setIdsurvey("1");
	        	x.setDescription(value.getDescription());
//	        	x.setUsername(value.getUsername());
	        	x.setUsername(value.getUsername());
	        	
	        	HttpEntity<GispdamPhotoCollect> request = new HttpEntity<>(x);
	        	ResponseEntity<GispdamResponse> response = restTemplate.
	        	  exchange("http://localhost:9000/api/gispdamPhotoCollect", HttpMethod.POST, request, GispdamResponse.class);

	        	if(response.getStatusCode() == HttpStatus.OK){
	        		value.setStatus("COMPLETE");
	        		if(!value.getType().contentEquals("")){
	        			value.setServerId(response.getBody().getId());
	        			value.setFileName(response.getBody().getFileName());
	        		}
	        		locationRepository.save(value);
	        	}
        	}
        	else{
        		log.debug("LOCATION ID : "+value.getId()+"| TYPE IS NULL");
        	}
    	});
    	
    	/*
    	List<Location> locationUpdate = locationRepository.findAllByStatus("UPDATE");
    	locationUpdate.forEach((value) -> {
    		if(value.getServerId()!=null){
	    		if(!value.getServerId().contentEquals("")){
	    			log.debug("UPDATE IDHEADER : "+value.getServerId());
	    			log.debug("UPDATE FILENAME : "+value.getFileName());
	    			
		    		GispdamPhotoCollect x = new GispdamPhotoCollect();
		    		x.setId(value.getServerId());
		    		x.setType(value.getType());		    		
		        	x.setServerId(value.getServerId());
		        	x.setFileName(value.getFileName());
		    		if(x.getType().contentEquals("PICTURE") || x.getType().contentEquals("VIDEO") || x.getType().contentEquals("ASBUILD")){
		    			log.debug("UPDATE TYPE PICTURE || VIDEO || ASBUILD");
		        		x.setDescription(value.getDescription());    				        		
		    		}
		    		else if(x.getType().contentEquals("MASALAH")){
		    			log.debug("UPDATE TYPE MASALAH");
		            	x.setMasalah(value.getMasalah());
		            	x.setUsulan(value.getUsulan());
		    		}
		    		
		    		log.debug("BEFORE SEND UPDATE REQUEST");
		        	HttpEntity<GispdamPhotoCollect> request = new HttpEntity<>(x);
		        	ResponseEntity<GispdamResponse> response = restTemplate.
		        	  exchange("http://localhost:9000/api/gispdamPhotoCollect", HttpMethod.POST, request, GispdamResponse.class);
	
		        	if(response.getStatusCode() == HttpStatus.OK){
		        		value.setStatus("COMPLETE");
	//	        		if(!value.getType().contentEquals("")){
	//	        			value.setServerId(response.getBody().getId());
	//	        		}
		        		locationRepository.save(value);
		        	}
		        	else{
		    			log.debug("UPDATE STATUS : "+response.getStatusCode());
		        	}
	    		}
    		}
    		else{
    			log.debug("UPDATE SERVER ID NULL");
    		}
    	});
    	*/
	}
}
