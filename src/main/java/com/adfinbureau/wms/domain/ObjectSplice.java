package com.adfinbureau.wms.domain;

public class ObjectSplice {
	private int index;
	private byte[] value;
	
	public ObjectSplice() {
		// TODO Auto-generated constructor stub
	}
	
	public ObjectSplice(int index, byte[] value) {
		// TODO Auto-generated constructor stub
		this.index = index;
		this.value = value;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public byte[] getValue() {
		return value;
	}

	public void setValue(byte[] value) {
		this.value = value;
	}
	
}
