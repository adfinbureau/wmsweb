package com.adfinbureau.wms.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "report_android_error")
public class AndroidLog {
    @Id
	private String id;
	private String APP_VERSION_CODE;
	private String ANDROID_VERSION;
	private String PACKAGE_NAME;
//	private String APP_VERSION_NAME;
	private String USER_CRASH_DATE;
	private String STACK_TRACE;
	private String USER_APP_START_DATE;
	private String REPORT_ID;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAPP_VERSION_CODE() {
		return APP_VERSION_CODE;
	}
	public void setAPP_VERSION_CODE(String aPP_VERSION_CODE) {
		APP_VERSION_CODE = aPP_VERSION_CODE;
	}
	public String getANDROID_VERSION() {
		return ANDROID_VERSION;
	}
	public void setANDROID_VERSION(String aNDROID_VERSION) {
		ANDROID_VERSION = aNDROID_VERSION;
	}
	public String getPACKAGE_NAME() {
		return PACKAGE_NAME;
	}
	public void setPACKAGE_NAME(String pACKAGE_NAME) {
		PACKAGE_NAME = pACKAGE_NAME;
	}
//	public String getAPP_VERSION_NAME() {
//		return APP_VERSION_NAME;
//	}
//	public void setAPP_VERSION_NAME(String aPP_VERSION_NAME) {
//		APP_VERSION_NAME = aPP_VERSION_NAME;
//	}
	public String getUSER_CRASH_DATE() {
		return USER_CRASH_DATE;
	}
	public void setUSER_CRASH_DATE(String uSER_CRASH_DATE) {
		USER_CRASH_DATE = uSER_CRASH_DATE;
	}
	public String getSTACK_TRACE() {
		return STACK_TRACE;
	}
	public void setSTACK_TRACE(String sTACK_TRACE) {
		STACK_TRACE = sTACK_TRACE;
	}
	public String getUSER_APP_START_DATE() {
		return USER_APP_START_DATE;
	}
	public void setUSER_APP_START_DATE(String uSER_APP_START_DATE) {
		USER_APP_START_DATE = uSER_APP_START_DATE;
	}
	public String getREPORT_ID() {
		return REPORT_ID;
	}
	public void setREPORT_ID(String rEPORT_ID) {
		REPORT_ID = rEPORT_ID;
	}
	@Override
	public String toString() {
		return "ReportAndroidError [id=" + id + ", APP_VERSION_CODE=" + APP_VERSION_CODE + ", ANDROID_VERSION="
				+ ANDROID_VERSION + ", PACKAGE_NAME=" + PACKAGE_NAME + ", USER_CRASH_DATE=" + USER_CRASH_DATE
				+ ", STACK_TRACE=" + STACK_TRACE + ", USER_APP_START_DATE=" + USER_APP_START_DATE + ", REPORT_ID="
				+ REPORT_ID + "]";
	}

	
	
	/*static class Build{
		private String board;
		private String serial;
		private String cpuAbi2;
		private String device;
	}*/
	
}
