package com.adfinbureau.wms.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * A Location.
 */

@Document(collection = "location")
public class Location extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("longitude")
    private String longitude;

    @Field("latitude")
    private String latitude;

    @Field("altitude")
    private String altitude;

    @Field("picture")
    private byte[] picture;

    @Field("picture_content_type")
    private String pictureContentType;

    @Field("video")
    private byte[] video;

    @Field("video_content_type")
    private String videoContentType;

    @Field("asbuild")
    private byte[] asbuild;

    @Field("asbuild_content_type")
    private String asbuildContentType;

    @Field("text")
    private String text;

    @Field("status")
    private String status;

    @Field("description")
    private String description;

    @Field("picture_splice")
    private Map<Integer,byte[]> pictureSplice = new HashMap<Integer,byte[]>();
    
    @Field("picture_splice_count")
    private int pictureSpliceCount;
    
    @Field("video_splice")
    private Map<Integer,byte[]> videoSplice = new HashMap<Integer,byte[]>();

    @Field("video_splice_count")
    private int videoSpliceCount;

    @Field("asbuild_splice")
    private Map<Integer,byte[]> asbuildSplice = new HashMap<Integer,byte[]>();

    @Field("asbuild_splice_count")
    private int asbuildSpliceCount;

    @Field("username")
    private String username;
    
    @Field("masalah")
    private String masalah;

    @Field("usulan")
    private String usulan;

    @Field("type")
    private String type;

    @Field("server_id")
    private String serverId;
    
    @Field("device_name")
    private String deviceName;
    
    @Field("file_name")
    private String fileName;
    
    @Field("version")
    private String version;
    
    private ZonedDateTime createdDate = ZonedDateTime.now();
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getAltitude() {
		return altitude;
	}
    
    public void setAltitude(String altitude) {
		this.altitude = altitude;
	}
    
    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public String getPictureContentType() {
        return pictureContentType;
    }

    public void setPictureContentType(String pictureContentType) {
        this.pictureContentType = pictureContentType;
    }

    public byte[] getVideo() {
        return video;
    }

    public void setVideo(byte[] video) {
        this.video = video;
    }

    public String getVideoContentType() {
        return videoContentType;
    }

    public void setVideoContentType(String videoContentType) {
        this.videoContentType = videoContentType;
    }

    public byte[] getAsbuild() {
        return asbuild;
    }

    public void setAsbuild(byte[] asbuild) {
        this.asbuild = asbuild;
    }

    public String getAsbuildContentType() {
        return asbuildContentType;
    }

    public void setAsbuildContentType(String asbuildContentType) {
        this.asbuildContentType = asbuildContentType;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map<Integer, byte[]> getPictureSplice() {
		return pictureSplice;
	}
    
    public void setPictureSplice(Map<Integer,byte[]> pictureSplice) {
		this.pictureSplice = pictureSplice;
	}
    
    public int getPictureSpliceCount() {
		return pictureSpliceCount;
	}
    
    public void setPictureSpliceCount(int pictureSpliceCount) {
		this.pictureSpliceCount = pictureSpliceCount;
	}
    
    public int getVideoSpliceCount() {
		return videoSpliceCount;
	}
    
    public void setVideoSpliceCount(int videoSpliceCount) {
		this.videoSpliceCount = videoSpliceCount;
	}
    
    public Map<Integer, byte[]> getVideoSplice() {
		return videoSplice;
	}
    
    public void setVideoSplice(Map<Integer, byte[]> videoSplice) {
		this.videoSplice = videoSplice;
	}
    
    public Map<Integer, byte[]> getAsbuildSplice() {
		return asbuildSplice;
	}

	public void setAsbuildSplice(Map<Integer, byte[]> asbuildSplice) {
		this.asbuildSplice = asbuildSplice;
	}

	public int getAsbuildSpliceCount() {
		return asbuildSpliceCount;
	}

	public void setAsbuildSpliceCount(int asbuildSpliceCount) {
		this.asbuildSpliceCount = asbuildSpliceCount;
	}

	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getMasalah() {
		return masalah;
	}
	public void setMasalah(String masalah) {
		this.masalah = masalah;
	}
	
	public String getUsulan() {
		return usulan;
	}
	
	public void setUsulan(String usulan) {
		this.usulan = usulan;
	}

	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getServerId() {
		return serverId;
	}
	
	public void setServerId(String serverId) {
		this.serverId = serverId;
	}
	
	@Override
	public ZonedDateTime getCreatedDate() {
		// TODO Auto-generated method stub
		return super.getCreatedDate();
	}
	
	@Override
	public void setCreatedDate(ZonedDateTime createdDate) {
		// TODO Auto-generated method stub
		super.setCreatedDate(createdDate);
	}
	
	public String getDeviceName() {
		return deviceName;
	}
	
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public String getFileName() {
		return fileName;
	}
	
	public String getVersion() {
		return version;
	}
	
	public void setVersion(String version) {
		this.version = version;
	}
	
	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Location location = (Location) o;
        if(location.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, location.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

	@Override
	public String toString() {
		return "Location [id=" + id + ", longitude=" + longitude + ", latitude=" + latitude 
//				", picture="+ Arrays.toString(picture) 
				+ ", pictureContentType=" + pictureContentType 
//				+ ", video="+ Arrays.toString(video) 
				+ ", videoContentType=" + videoContentType 
//				+ ", asbuild="+ Arrays.toString(asbuild) 
				+ ", asbuildContentType=" + asbuildContentType + ", text=" + text
				+ ", status=" + status + ", description=" + description + ", pictureSplice=" + pictureSplice
				+ ", pictureSpliceCount=" + pictureSpliceCount + ", videoSplice=" + videoSplice + ", videoSpliceCount="
				+ videoSpliceCount + ", asbuildSplice=" + asbuildSplice + ", asbuildSpliceCount=" + asbuildSpliceCount
				+ ", username=" + username + ", masalah=" + masalah + ", usulan=" + usulan + ", type=" + type
				+ ", serverId=" + serverId + ", deviceName=" + deviceName + ", fileName=" + fileName + ", version="
				+ version + ", createdDate=" + createdDate + "]";
	}

	

    
}
