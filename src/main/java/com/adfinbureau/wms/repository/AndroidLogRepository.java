package com.adfinbureau.wms.repository;

import com.adfinbureau.wms.domain.AndroidLog;
import com.adfinbureau.wms.domain.Location;
import com.adfinbureau.wms.domain.User;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Location entity.
 */
@SuppressWarnings("unused")
public interface AndroidLogRepository extends MongoRepository<AndroidLog,String> {
}
