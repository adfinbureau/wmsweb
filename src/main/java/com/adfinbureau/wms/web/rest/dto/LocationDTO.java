package com.adfinbureau.wms.web.rest.dto;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

public class LocationDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;
    private String longitude;
    private String latitude;
    private String altitude;
    private byte[] picture;
    private String pictureContentType;
    private byte[] video;
    private String videoContentType;
    private byte[] asbuild;
    private String asbuildContentType;
    private String text;
    private String status;
    private String description;
    
    private byte[] pictureSplice;
    private String pictureSpliceIndex;
    private int pictureSpliceCount;
    
    private byte[] videoSplice;
    private String videoSpliceIndex;
    private int videoSpliceCount;
    
    private byte[] asbuildSplice;
    private String asbuildSpliceIndex;
    private int asbuildSpliceCount;
    
    private String username;
    private String masalah;
    private String usulan;
    
    private String deviceName;
    private String version;
    private String fileName;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }
    
    public String getAltitude() {
		return altitude;
	}
    
    public void setAltitude(String altitude) {
		this.altitude = altitude;
	}

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public String getPictureContentType() {
        return pictureContentType;
    }

    public void setPictureContentType(String pictureContentType) {
        this.pictureContentType = pictureContentType;
    }

    public byte[] getVideo() {
        return video;
    }

    public void setVideo(byte[] video) {
        this.video = video;
    }

    public String getVideoContentType() {
        return videoContentType;
    }

    public void setVideoContentType(String videoContentType) {
        this.videoContentType = videoContentType;
    }

    public byte[] getAsbuild() {
        return asbuild;
    }

    public void setAsbuild(byte[] asbuild) {
        this.asbuild = asbuild;
    }

    public String getAsbuildContentType() {
        return asbuildContentType;
    }

    public void setAsbuildContentType(String asbuildContentType) {
        this.asbuildContentType = asbuildContentType;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getPictureSplice() {
		return pictureSplice;
	}
    
    public void setPictureSplice(byte[] pictureSplice) {
		this.pictureSplice = pictureSplice;
	}
    
    public String getPictureSpliceIndex() {
		return pictureSpliceIndex;
	}
    
    public void setPictureSpliceIndex(String pictureSpliceIndex) {
		this.pictureSpliceIndex = pictureSpliceIndex;
	}
    
    public int getPictureSpliceCount() {
		return pictureSpliceCount;
	}
    
    public void setPictureSpliceCount(int pictureSpliceCount) {
		this.pictureSpliceCount = pictureSpliceCount;
	}
    
    
    public byte[] getVideoSplice() {
		return videoSplice;
	}

	public void setVideoSplice(byte[] videoSplice) {
		this.videoSplice = videoSplice;
	}

	public String getVideoSpliceIndex() {
		return videoSpliceIndex;
	}

	public void setVideoSpliceIndex(String videoSpliceIndex) {
		this.videoSpliceIndex = videoSpliceIndex;
	}

	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}

	public byte[] getAsbuildSplice() {
		return asbuildSplice;
	}

	public void setAsbuildSplice(byte[] asbuildSplice) {
		this.asbuildSplice = asbuildSplice;
	}

	public String getAsbuildSpliceIndex() {
		return asbuildSpliceIndex;
	}

	public void setAsbuildSpliceIndex(String asbuildSpliceIndex) {
		this.asbuildSpliceIndex = asbuildSpliceIndex;
	}

	public int getAsbuildSpliceCount() {
		return asbuildSpliceCount;
	}

	public void setAsbuildSpliceCount(int asbuildSpliceCount) {
		this.asbuildSpliceCount = asbuildSpliceCount;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LocationDTO location = (LocationDTO) o;
        if(location.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, location.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

	@Override
	public String toString() {
		return "LocationDTO [id=" + id + ", longitude=" + longitude + ", latitude=" + latitude + ", picture="
				+ Arrays.toString(picture) + ", pictureContentType=" + pictureContentType + ", video="
				+ Arrays.toString(video) + ", videoContentType=" + videoContentType + ", asbuild="
				+ Arrays.toString(asbuild) + ", asbuildContentType=" + asbuildContentType + ", text=" + text
				+ ", status=" + status + ", description=" + description + ", pictureSplice="
				+ Arrays.toString(pictureSplice) + ", pictureSpliceIndex=" + pictureSpliceIndex
				+ ", pictureSpliceCount=" + pictureSpliceCount + ", videoSplice=" + Arrays.toString(videoSplice)
				+ ", videoSpliceIndex=" + videoSpliceIndex + ", videoSpliceCount=" + videoSpliceCount
				+ ", asbuildSplice=" + Arrays.toString(asbuildSplice) + ", asbuildSpliceIndex=" + asbuildSpliceIndex
				+ ", asbuildSpliceCount=" + asbuildSpliceCount + ", username=" + username + ", masalah=" + masalah
				+ ", usulan=" + usulan + ", deviceName=" + deviceName + ", version=" + version + ", fileName="
				+ fileName + "]";
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public int getVideoSpliceCount() {
		// TODO Auto-generated method stub
		return videoSpliceCount;
	}

	public void setVideoSpliceCount(int videoSpliceCount) {
		this.videoSpliceCount = videoSpliceCount;
	}
	public String getMasalah() {
		return masalah;
	}
	public void setMasalah(String masalah) {
		this.masalah = masalah;
	}
	
	public String getUsulan() {
		return usulan;
	}
	
	public void setUsulan(String usulan) {
		this.usulan = usulan;
	}
}
