/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.adfinbureau.wms.web.rest.dto;
