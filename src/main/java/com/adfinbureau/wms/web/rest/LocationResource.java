package com.adfinbureau.wms.web.rest;

import java.io.ByteArrayOutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.adfinbureau.wms.domain.Location;
import com.adfinbureau.wms.repository.LocationRepository;
import com.adfinbureau.wms.web.rest.dto.LocationDTO;
import com.adfinbureau.wms.web.rest.util.HeaderUtil;
import com.adfinbureau.wms.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;

/**
 * REST controller for managing Location.
 */
@RestController
@RequestMapping("/api")
public class LocationResource {

    private final Logger log = LoggerFactory.getLogger(LocationResource.class);
        
    @Inject
    private LocationRepository locationRepository;
    
    /**
     * POST  /locations : Create a new location.
     *
     * @param location the location to create
     * @return the ResponseEntity with status 201 (Created) and with body the new location, or with status 400 (Bad Request) if the location has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/locations",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Location> createLocation(@RequestBody Location location) throws URISyntaxException {
        log.debug("REST request to save Location : {}", location);
        if (location.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("location", "idexists", "A new location cannot already have an ID")).body(null);
        }
        Location result = locationRepository.save(location);
        return ResponseEntity.created(new URI("/api/locations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("location", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /locations : Updates an existing location.
     *
     * @param location the location to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated location,
     * or with status 400 (Bad Request) if the location is not valid,
     * or with status 500 (Internal Server Error) if the location couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/locations",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Location> updateLocation(@RequestBody LocationDTO locationDTO) throws URISyntaxException {
        log.debug("REST request to update Location : {}", locationDTO);

        try{
	        Location currentLocation = locationRepository.findOne(locationDTO.getId());
	        log.debug("LocationResource "+currentLocation);
	        if(currentLocation!=null){        	
	        	//PICTURE
	        	if(locationDTO.getPictureSplice() != null){
			        Map<Integer, byte[]> splice = currentLocation.getPictureSplice();        
			        if(splice.containsKey(locationDTO.getPictureSpliceIndex())){
			        	splice.remove(locationDTO.getPictureSpliceIndex());
			        	splice.put(Integer.valueOf(locationDTO.getPictureSpliceIndex()), locationDTO.getPictureSplice());        }
			        else{
			        	splice.put(Integer.valueOf(locationDTO.getPictureSpliceIndex()), locationDTO.getPictureSplice());
			        }
			        
			        if(currentLocation.getPictureSpliceCount() == splice.size()){
			    		ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
			    		Map<Integer, byte[]> treeMap = new TreeMap<Integer, byte[]>(splice);
			    		treeMap.forEach((k, d) -> {
			        		log.debug("KEY : "+k);
			        		try {
								outputStream.write( d );
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
			        	});
			    		byte c[] = outputStream.toByteArray();
			    		currentLocation.setPicture(c);
			    		currentLocation.getPictureSplice().clear();
			        }
			        if(locationDTO.getPictureSpliceCount() != 0){
			        	currentLocation.setPictureSpliceCount(locationDTO.getPictureSpliceCount());
			        }
	        	}
		        
	        	//VIDEO
	        	if(locationDTO.getVideoSplice() != null){
	        		Map<Integer, byte[]> splice = currentLocation.getVideoSplice();        
			        if(splice.containsKey(locationDTO.getVideoSpliceIndex())){
			        	splice.remove(locationDTO.getVideoSpliceIndex());
			        	splice.put(Integer.valueOf(locationDTO.getVideoSpliceIndex()), locationDTO.getVideoSplice());        }
			        else{
			        	splice.put(Integer.valueOf(locationDTO.getVideoSpliceIndex()), locationDTO.getVideoSplice());
			        }
			        
			        if(currentLocation.getVideoSpliceCount() == splice.size()){
			    		ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
			    		Map<Integer, byte[]> treeMap = new TreeMap<Integer, byte[]>(splice);
			    		treeMap.forEach((k, d) -> {
			        		try {
								outputStream.write( d );
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
			        	});
			    		byte c[] = outputStream.toByteArray();
			    		currentLocation.setVideo(c);
			    		currentLocation.getVideoSplice().clear();
			        }
			        if(locationDTO.getVideoSpliceCount() != 0){
			        	currentLocation.setVideoSpliceCount(locationDTO.getVideoSpliceCount());
			        }
	        	}
	        	
	        	//ASBUILD
	        	if(locationDTO.getAsbuildSplice() != null){
	        		Map<Integer, byte[]> splice = currentLocation.getAsbuildSplice();        
			        if(splice.containsKey(locationDTO.getAsbuildSpliceIndex())){
			        	splice.remove(locationDTO.getAsbuildSpliceIndex());
			        	splice.put(Integer.valueOf(locationDTO.getAsbuildSpliceIndex()), locationDTO.getAsbuildSplice());        }
			        else{
			        	splice.put(Integer.valueOf(locationDTO.getAsbuildSpliceIndex()), locationDTO.getAsbuildSplice());
			        }
			        
			        if(currentLocation.getAsbuildSpliceCount() == splice.size()){
			    		ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
			    		Map<Integer, byte[]> treeMap = new TreeMap<Integer, byte[]>(splice);
			    		treeMap.forEach((k, d) -> {
			        		try {
								outputStream.write( d );
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
			        	});
			    		byte c[] = outputStream.toByteArray();
			    		currentLocation.setAsbuild(c);
			    		currentLocation.getAsbuildSplice().clear();
			        }
			        if(locationDTO.getAsbuildSpliceCount() != 0){
			        	currentLocation.setAsbuildSpliceCount(locationDTO.getAsbuildSpliceCount());
			        }
	        	}
	        	
	        	if(locationDTO.getDescription() != null){
	        		currentLocation.setDescription(locationDTO.getDescription());
	        	}
				if(locationDTO.getUsername() != null){
	        		currentLocation.setUsername(locationDTO.getUsername());
	        	}
				if(locationDTO.getMasalah() != null){
					currentLocation.setMasalah(locationDTO.getMasalah());
					currentLocation.setUsulan(locationDTO.getUsulan());
				}
				if(locationDTO.getDeviceName() != null){
					currentLocation.setDeviceName(locationDTO.getDeviceName());
				}
				if(locationDTO.getVersion() != null){
					currentLocation.setVersion(locationDTO.getVersion());
				}
				if(currentLocation.getStatus().contentEquals("COMPLETE")){
					currentLocation.setStatus("UPDATE");
				}
				
		        log.debug("LocationResource BEFORE SAVE : "+currentLocation);
		        Location result = locationRepository.save(currentLocation);
		        log.debug("LocationResource AFTER SAVE : "+currentLocation);
		        
		        return ResponseEntity.ok()
		                .headers(HeaderUtil.createEntityUpdateAlert("location", currentLocation.getId().toString()))
		                .body(result);
	        }   
        }catch(Exception e){
            return ResponseEntity.badRequest().body(null);
        }
        return ResponseEntity.badRequest().body(null);
    }

    byte[] concatenateByteArrays(byte[] a, byte[] b) {
        byte[] result = new byte[a.length + b.length]; 
        System.arraycopy(a, 0, result, 0, a.length); 
        System.arraycopy(b, 0, result, a.length, b.length); 
        return result;
    } 
    /**
     * GET  /locations : get all the locations.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of locations in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/locations",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Location>> getAllLocations(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Locations");
        Page<Location> page = locationRepository.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/locations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /locations/:id : get the "id" location.
     *
     * @param id the id of the location to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the location, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/locations/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Location> getLocation(@PathVariable String id) {
        log.debug("REST request to get Location : {}", id);
        Location location = locationRepository.findOne(id);
        return Optional.ofNullable(location)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /locations/:id : delete the "id" location.
     *
     * @param id the id of the location to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/locations/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteLocation(@PathVariable String id) {
        log.debug("REST request to delete Location : {}", id);
        locationRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("location", id.toString())).build();
    }

}
