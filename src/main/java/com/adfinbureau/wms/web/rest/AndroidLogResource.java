package com.adfinbureau.wms.web.rest;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.adfinbureau.wms.repository.AndroidLogRepository;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.util.JSON;

/**
 * Controller for view and managing Log Level at runtime.
 */
@RestController
@RequestMapping("/api/android")
public class AndroidLogResource {

    private final Logger log = LoggerFactory.getLogger(AndroidLogResource.class);

    @Inject
    private Mongo mongo;

    @Inject
    private MongoProperties mongoProperties;

    @Inject
    private AndroidLogRepository androidLogRepository;
    
    @RequestMapping(value = "/logs", method = RequestMethod.POST, produces="application/json", consumes="application/json")
    @ResponseBody
    public String post(@RequestBody String json) {
//    	try {
//			JSONObject jsonObj = new JSONObject(json);
//			String androidVersion = jsonObj.getString("ANDROID_VERSION");
//			String stackTrace = jsonObj.getString("STACK_TRACE");
//			AndroidLog err = new AndroidLog();
//			err.setANDROID_VERSION(androidVersion);
//			err.setSTACK_TRACE(stackTrace);
//			androidLogRepository.save(err);
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
////			System.out.println(e.getMessage());
//		}
//    	ObjectMapper mapper = new ObjectMapper();
//    	try {
//			ReportAndroidError reportAndroid = mapper.readValue(json, ReportAndroidError.class);
//	    	System.out.println(reportAndroid);
//	    	System.out.println(json);
//		} catch (JsonParseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			System.out.println("JSON PARSE EXCEPTION");
//		} catch (JsonMappingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			System.out.println("JSON MAPPING EXCEPTION");
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			System.out.println("JSON IO EXCEPTION");
//		}
//    	JSONObject jsonObj;
//		try {
//			jsonObj = new JSONObject(json);
//	    	System.out.println(jsonObj.toString(4)); // Print it with specified indentation
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} // Convert text to object
//    	log.debug(json);
//    	System.out.println(json);
//        POJO pj = new POJO();
//        ObjectMapper mapper = new ObjectMapper();
//        pj = mapper.readValue(json, POJO.class);

//    	if (json.contains(".")) {
//    		json = json.replaceAll("." , "\\u002e");
//    	    tagsInfo.put(tag.replace(".", "\\u002e"), "1");
//    	} else {
//    	    tagsInfo.put(tag, "1");
//    	}
    	
    	DBObject dbObject = (DBObject) JSON.parse(json);    	
		DB db = mongo.getDB(mongoProperties.getDatabase());
		DBCollection collection = db.getCollection("android_log");
		collection.insert(dbObject);
        //do some things with json, put some header information in json
        return json.toString();
    }
}
