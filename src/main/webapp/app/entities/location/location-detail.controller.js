(function() {
    'use strict';

    angular
        .module('wmsApp')
        .controller('LocationDetailController', LocationDetailController);

    LocationDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'DataUtils', 'entity', 'Location'];

    function LocationDetailController($scope, $rootScope, $stateParams, DataUtils, entity, Location) {
        var vm = this;
        vm.location = entity;
        
        var unsubscribe = $rootScope.$on('wmsApp:locationUpdate', function(event, result) {
            vm.location = result;
        });
        $scope.$on('$destroy', unsubscribe);

        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;
    }
})();
